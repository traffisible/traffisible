package com.example.ripzery.traffisible.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by ripzery on 11/23/2014.
 */
/*
    เป็น BroadcastReceiver ที่ส่ง service ให้ service fetch news มาเก็บลงใน database ทำงาน
 */
public class MyAlarmReceiver extends BroadcastReceiver {

    public static final int REQUEST_CODE = 12345;
    public static final String ACTION = "com.example.ripzery.traffisible";
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, UpdateNewsService.class);
        context.startService(i);
    }

}
