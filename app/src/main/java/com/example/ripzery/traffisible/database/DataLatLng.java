package com.example.ripzery.traffisible.database;

import com.google.android.gms.maps.model.LatLng;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Nathadanai_C on 11/24/14.
 */

/*
    Use to keep the recording user trip as an object
 */
public class DataLatLng {
    public String _name;
    public ArrayList<LatLng> _Latlng;
    public String _distance;
    public String _date;
    public String _time;

    public DataLatLng(String name, ArrayList<LatLng> Latlng, String distance,String date, String time){
        this._name=name;
        this._Latlng=Latlng;
        this._distance=distance;
        this._date = date;
        this._time = time;
    }

    public DataLatLng(String name, String distance,String date, String time){
        this._name=name;
        this._distance=distance;
        this._date = date;
        this._time = time;
    }

    public String getName(){
        return this._name;
    }

    public ArrayList<LatLng> getLatlng(){
        return this._Latlng;
    }

    public String get_Distance(){
        return this._distance;
    }

    public String get_date(){
        return _date;
    }

    public String get_time(){
        return _time;
    }
}
