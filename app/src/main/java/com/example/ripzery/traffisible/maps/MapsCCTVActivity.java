package com.example.ripzery.traffisible.maps;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.MenuItem;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.example.ripzery.traffisible.R;
import com.example.ripzery.traffisible.utils.TypefaceSpan;
import com.example.ripzery.traffisible.jsonclass.CCTV;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

/*
    แสดงตำแหน่งของกล้อง CCTV บน Google Map พร้อมกับแสดงภาพจากกล้อง CCTV
 */
public class MapsCCTVActivity extends ActionBarActivity {
    // TODO: Rename parameter arguments, choose names that match
    private final LatLngBounds Thailand = new LatLngBounds(new LatLng(5.371270, 97.859916), new LatLng(19.680066, 104.957083));
    private String mName, mURL;
    private String mLatitude, mLongitude;
    private GoogleMap mMap;
    private CCTV cctv;
    private ArrayList<String> urlList;
    private android.support.v7.app.ActionBar mActionBar;
    private SliderLayout mDemoSlider;

    public MapsCCTVActivity() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_cctv_maps);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setHomeAsUpIndicator(R.drawable.ic_ab_up_compat);
        setTitle(" TRAFFISIBLE");
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mName = bundle.getString("name");
            mURL = bundle.getString("img");
            urlList = new ArrayList<String>();
            for(String url : mURL.split(",")){
                urlList.add(url+"&format=jpg");
            }
            mLatitude = bundle.getString("lat");
            mLongitude = bundle.getString("lng");
        }


        mDemoSlider = (SliderLayout)findViewById(R.id.slider);
        HashMap<String,String> url_maps = new HashMap<String, String>();
        int index = 1;
        for(String mUrl : urlList){
            url_maps.put(mName+ " No." + index,mUrl);
            index ++;
        }

        for(String name : url_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .description(mName)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit);

            //add your extra information
            textSliderView.getBundle()
                    .putString("extra",name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);

        setUpMapIfNeeded();
        if (mName.equals("Rain Detection Radar")) {
            mLatitude = "13.725518";
            mLongitude = "100.522904";
        }
        final LatLng latlng = new LatLng(Double.parseDouble(mLatitude), Double.parseDouble(mLongitude));
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
//                setUpMap();
                if (mName.equals("Rain Detection Radar")) {
                    setCameraPosition(latlng, 8);
                } else
                    setCameraPosition(latlng, 14);

                MarkerOptions marker;
                marker = new MarkerOptions().position(latlng).title(mName);
                mMap.addMarker(marker);
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setCameraPosition(LatLng Location, int zoomLevel) {
        CameraPosition camPos = new CameraPosition.Builder().target(Location).zoom(zoomLevel).tilt(45).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camPos));
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mMap.clear();
    }



    public void setSubTitle(String subTitle) {
        SpannableString mStringSubTitle = new SpannableString(subTitle);
        mStringSubTitle.setSpan(new TypefaceSpan(this, "Roboto-Light.ttf"), 0, mStringSubTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setSubtitle(mStringSubTitle);
    }

    @Override
    public void setTitle(CharSequence title) {
        SpannableString mStringTitle = new SpannableString(title);
        mStringTitle.setSpan(new TypefaceSpan(this, "Roboto-Medium.ttf"), 0, mStringTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(mStringTitle);
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map2))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
//                setUpMap();
            }
        }
    }

    public void setUpMap() {
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(Thailand, 3));
    }
}
