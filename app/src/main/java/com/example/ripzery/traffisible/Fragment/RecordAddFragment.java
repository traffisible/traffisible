package com.example.ripzery.traffisible.fragment;
import java.util.ArrayList;

import com.example.ripzery.traffisible.MyActivity;
import com.example.ripzery.traffisible.R;
import com.example.ripzery.traffisible.adapter.RecordTripCardAdapter;
import com.example.ripzery.traffisible.database.DataLatLng;
import com.example.ripzery.traffisible.database.MyDbHelper;
import com.example.ripzery.traffisible.maps.RecordMapFragmentActivity;
import com.getbase.floatingactionbutton.AddFloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionButton;
//import com.melnykov.fab.FloatingActionButton;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

/**
 * Created by Nathadanai_C on 11/26/14.
 */

/*
    User can add desired trip to database in this fragment. The detail of each trip can be found here which
    are displayed using CardView. Deleting unwanted trip is available as well.
 */
public class RecordAddFragment extends Fragment {
    private EditText mAddRecordBox;
    private AddFloatingActionButton mAddRecordbtn;
    private String tripname;
    private ArrayList<DataLatLng> listRecords;
    private RecordTripCardAdapter adapter;
    private RecyclerView mRecyclerView;
    private MyDbHelper mDbHelper;
    private int requestCode = 123;
    private MyActivity myActivity;
    private View mRootView;
    private SQLiteDatabase mDb;
    private LinearLayoutManager mLayoutManager;
    private AlertDialog.Builder builder;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        myActivity = (MyActivity) activity;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.fragment_addrecord);
        mDbHelper = new MyDbHelper(myActivity);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_addrecord, container, false);

        if (savedInstanceState == null) {
            mRootView = view;

            builder = new AlertDialog.Builder(getActivity());
            mAddRecordbtn = (AddFloatingActionButton) mRootView.findViewById(R.id.btnNewRecord);
            mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recordRecyclerView);
            mRecyclerView.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(myActivity);
            mRecyclerView.setLayoutManager(mLayoutManager);
            listRecords = new ArrayList<DataLatLng>();

            listRecords=mDbHelper.getAllRecordDataLatLng();
            Log.d("Fetch Add Record Database", "Successfully");


            // use your custom layout when hit the save button
            adapter = new RecordTripCardAdapter(myActivity, listRecords);
            mRecyclerView.setAdapter(adapter);

            mAddRecordbtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                // TODO Auto-generated method stub

                final LayoutInflater inflater = getActivity().getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_addrecord, null);
                builder.setView(dialogView)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mAddRecordBox = (EditText) dialogView.findViewById(R.id.tripname);
                                tripname=mAddRecordBox.getText().toString();
                                Intent intent = new Intent( myActivity,RecordMapFragmentActivity.class);
                                intent.putExtra("tripname", tripname);
                                startActivityForResult(intent,requestCode);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                builder.setTitle("Enter your trip name");
                builder.create();
                builder.show();
                }
            });
        }
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == myActivity.RESULT_OK){
            listRecords = mDbHelper.getAllRecordDataLatLng();
            adapter = new RecordTripCardAdapter(myActivity, listRecords);
            mRecyclerView.setAdapter(adapter);
            ((PreferFragment)myActivity.preferFlag).setPreference();
        }else
            Toast.makeText(myActivity, "No action has been performed", Toast.LENGTH_LONG).show();
    }

}
