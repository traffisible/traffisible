package com.example.ripzery.traffisible.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.ripzery.traffisible.MyActivity;
import com.example.ripzery.traffisible.R;
import com.example.ripzery.traffisible.database.DataLatLng;
import com.example.ripzery.traffisible.database.MyDbHelper;
import com.example.ripzery.traffisible.fragment.PreferFragment;
import com.example.ripzery.traffisible.maps.RecordViewActivity;


import java.util.ArrayList;

/**
 * Created by ripzery on 11/27/2014.
 */

/*
    CardAdapter for recorded trip. This CardAdapter will use in AddRecordFragment to show user recorded trip
 */
public class RecordTripCardAdapter extends RecyclerView.Adapter<RecordTripCardAdapter.ViewHolder>{
    private static MyActivity myActivity = null;
    private static ArrayList<DataLatLng> listRecord = null;


    public RecordTripCardAdapter(MyActivity context, ArrayList<DataLatLng> objects){
        myActivity = context;
        listRecord = objects;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvTitle;
        TextView tvDistance;
        TextView tvDate;
        TextView tvTime;
        Button btnRemove;
        CardView container;
        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tvRecordName);
            tvDistance = (TextView) itemView.findViewById(R.id.tvRecordDistance);
            tvDate = (TextView) itemView.findViewById(R.id.tvRecordDate);
            tvTime = (TextView) itemView.findViewById(R.id.tvRecordTime);
            container = (CardView) itemView.findViewById(R.id.recordCardView);
            btnRemove = (Button) itemView.findViewById(R.id.btnHide);

            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent trip = new Intent(myActivity, RecordViewActivity.class);
                    trip.putExtra("TripName",tvTitle.getText().toString());
                    myActivity.startActivity(trip);
                }
            });

            btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SQLiteDatabase mDb;
                    MyDbHelper myDbHelper = new MyDbHelper(myActivity);
                    mDb = myDbHelper.getWritableDatabase();
                    mDb.delete(MyDbHelper.TABLE_NAME5, MyDbHelper.COLUMN_NAME+"= '"+tvTitle.getText().toString()+"'",null);
                    listRecord.remove(getPosition());
                    SharedPreferences sharedPref = myActivity.getSharedPreferences("preferences", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putBoolean("isAllNews",true);
                    editor.putString("tripname", "notfound");
                    editor.putInt("spinPos", 0);
                    editor.commit();
                    ((PreferFragment)myActivity.preferFlag).setPreference();
                    RecordTripCardAdapter.this.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.record_cardview,viewGroup,false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Typeface tfBold = Typeface.createFromAsset(myActivity.getAssets(), "fonts/RSU_BOLD.ttf");
        Typeface tfMedium = Typeface.createFromAsset(myActivity.getAssets(), "fonts/RSU_Regular.ttf");
        Typeface tfLight = Typeface.createFromAsset(myActivity.getAssets(), "fonts/RSU_light.ttf");

        viewHolder.tvTitle.setText(listRecord.get(i).getName());
        viewHolder.tvDistance.setText("ระยะทางทั้งหมด : " + listRecord.get(i).get_Distance() + " เมตร");
        viewHolder.tvDate.setText("วันที่บันทึก : "+ listRecord.get(i).get_date());
        viewHolder.tvTime.setText("เวลาที่บันทึกเส้นทาง : " + listRecord.get(i).get_time() + " น.");

        viewHolder.tvTitle.setTypeface(tfBold);
        viewHolder.tvDistance.setTypeface(tfLight);
        viewHolder.tvDate.setTypeface(tfLight);
        viewHolder.tvTime.setTypeface(tfLight);

    }

    @Override
    public int getItemCount() {
        return listRecord.size();
    }
}
