package com.example.ripzery.traffisible.maps;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.MenuItem;

import com.example.ripzery.traffisible.R;
import com.example.ripzery.traffisible.database.MyDbHelper;
import com.example.ripzery.traffisible.utils.TypefaceSpan;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Nathadanai_C on 11/26/14.
 */

/*
    ไว้สำหรับการแสดงผลเส้นทางที่ถูกบันทึกไว้ โดยจะแสดงผลใน Google Map พร้อมกับมี Marker ชี้จุดเริ่มต้น - ปลายทาง
 */
public class RecordViewActivity extends ActionBarActivity {
    GoogleMap myMap;
    private String tripname;
    private MyDbHelper mDbHelper;
    private PolylineOptions mLine;
    private LatLngBounds.Builder b;
    private android.support.v7.app.ActionBar mActionBar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_viewactivity_layout);

        mDbHelper = new MyDbHelper(this);
        Intent intent = getIntent();
        tripname = intent.getStringExtra("TripName");

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setHomeAsUpIndicator(R.drawable.ic_ab_up_compat);
        setTitle(tripname);
        b = new LatLngBounds.Builder();
        SupportMapFragment mySupportMapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map_canvas);
        myMap=mySupportMapFragment.getMap();

        try {
            ArrayList<LatLng> latlng_Arr = mDbHelper.getLatLng(tripname);
            for(int i=0; i<latlng_Arr.size(); i++){
                b.include(latlng_Arr.get(i));
                if(i>0){
                    mLine = new PolylineOptions().width(4).color(Color.GREEN);
                    mLine.add(latlng_Arr.get(i),latlng_Arr.get(i-1));
                    myMap.addPolyline(mLine);
                }
            }
        myMap.addMarker(new MarkerOptions().position(latlng_Arr.get(0)).title("Start Point"));
        myMap.addMarker(new MarkerOptions().position(latlng_Arr.get(latlng_Arr.size()-1)).title("Destination"));
        myMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                LatLngBounds bounds = b.build();
                //Change the padding as per needed
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds , 20);
                myMap.animateCamera(cu);
            }
        });
//        myMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(15, 100), 8));



            //Calculate the markers to get their position

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        SpannableString mStringTitle = new SpannableString(title);
        mStringTitle.setSpan(new TypefaceSpan(this, "Roboto-Medium.ttf"), 0, mStringTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(mStringTitle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
