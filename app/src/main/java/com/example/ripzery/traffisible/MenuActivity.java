package com.example.ripzery.traffisible;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.ripzery.traffisible.database.MyDbHelper;
import com.github.amlcurran.showcaseview.ApiUtils;
import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ActionViewTarget;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Nathadanai_C on 11/27/14.
 */

/*
    เป็นหน้า Activity หน้าแรกที่เชื่อมโยงกับ Fragment หน้าต่างๆ
 */
public class MenuActivity extends Activity {

    private static final String APP_ID = "61d787a9";
    private static final String KEY = "jADjas9PXU";
    private int FlagStartUp = 0;
    private int BackgroundService = 0;
    private int NotiService = 0;
    private String passKey = "";
    private String newsJsonString, cctvJsonString;
    private SQLiteDatabase mDb;
    private JsonElement jsonElement;
    private String url, url2;
    private ImageButton tripNavigate_button;
    private ImageButton newsNavigate_button;
    private ImageButton camsNavigate_button;
    private ImageButton prefNavigate_button;
    private TextView title;
    private SQLiteDatabase mDatabase;
    private String jsonNews;
    private MyDbHelper myDbHelper;
    private ProgressDialog progress;
    private int numberOfLoad = 0;
    private Boolean isNewsUpdate = false;
    private final ApiUtils apiUtils = new ApiUtils();


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menus);

        //check if the database isn't empty.
        SharedPreferences sharedPref = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        isNewsUpdate =  sharedPref.getBoolean("isNewsUpdate",false);
        if(!isNewsUpdate){
            //Database is empty
            progress = ProgressDialog.show(this, "Initialize database","Please wait...", true);
            setPassKey();
            Log.d("database empty ","so let's initialize database");
        }

        if(FlagStartUp == 0){
            //Do something here
           FlagStartUp=1;
        }

        title = (TextView) findViewById(R.id.title);
        tripNavigate_button = (ImageButton) findViewById(R.id.imbtn1);
        newsNavigate_button = (ImageButton) findViewById(R.id.imbtn2);
        camsNavigate_button = (ImageButton) findViewById(R.id.imbtn3);
        prefNavigate_button = (ImageButton) findViewById(R.id.imbtn4);

        //Handle trip button press
        tripNavigate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this,MyActivity.class);
                intent.putExtra("selectItem",2);
                startActivity(intent);
            }
        });

        //Handle news button press
        newsNavigate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this,MyActivity.class);
                intent.putExtra("selectItem",0);
                startActivity(intent);
            }
        });

        //Handle camera button press
        camsNavigate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this,MyActivity.class);
                intent.putExtra("selectItem",1);
                startActivity(intent);

            }
        });

        //Handle preference button press
        prefNavigate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this,MyActivity.class);
                intent.putExtra("selectItem",3);
                startActivity(intent);
            }
        });

        ViewTarget target = new ViewTarget(R.id.imbtn1,this);
        new ShowcaseView.Builder(this)
                .setTarget(target)
                .setContentTitle("Add record button")
                .setContentText("Add your trip by pressing this button. Traffic news can be chosen to display according to your desired trip.")
                .hideOnTouchOutside()
                .singleShot(1)
                .setStyle(R.style.MyCustomTheme2)
                .setShowcaseEventListener(new OnShowcaseEventListener() {
                    @Override
                    public void onShowcaseViewHide(ShowcaseView showcaseView) {
                        unDimView(camsNavigate_button);
                        unDimView(title);
                        unDimView(newsNavigate_button);
                        unDimView(prefNavigate_button);
                        displayShowcaseViewTwo();
                    }

                    @Override
                    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

                    }

                    @Override
                    public void onShowcaseViewShow(ShowcaseView showcaseView) {
                        dimView(camsNavigate_button);
                        dimView(title);
                        dimView(newsNavigate_button);
                        dimView(prefNavigate_button);
                    }
                })
                .build();

    }

    public void displayShowcaseViewTwo(){
        ViewTarget target = new ViewTarget(R.id.imbtn2,this);
        new ShowcaseView.Builder(this)
                .setTarget(target)
                .setContentTitle("Incident News")
                .setContentText("Press here to see the latest traffic news. News display depends on the option you have selected in preference. All latest news will be shown as a default.")
                .hideOnTouchOutside()
                .singleShot(2)
                .setStyle(R.style.MyCustomTheme2)
                .setShowcaseEventListener(new OnShowcaseEventListener() {
                    @Override
                    public void onShowcaseViewHide(ShowcaseView showcaseView) {
                        unDimView(tripNavigate_button);
                        unDimView(title);
                        unDimView(camsNavigate_button);
                        unDimView(prefNavigate_button);
                        displayShowcaseViewThree();
                    }

                    @Override
                    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

                    }

                    @Override
                    public void onShowcaseViewShow(ShowcaseView showcaseView) {
                        dimView(tripNavigate_button);
                        dimView(title);
                        dimView(camsNavigate_button);
                        dimView(prefNavigate_button);
                    }
                })
                .build();
    }

    public void displayShowcaseViewThree(){
        ViewTarget target = new ViewTarget(R.id.imbtn3,this);
        new ShowcaseView.Builder(this)
                .setTarget(target)
                .setContentTitle("CCTV")
                .setContentText("Press here to watch a live feed from traffic cctv")
                .hideOnTouchOutside()
                .singleShot(3)
                .setStyle(R.style.MyCustomTheme)
                .setShowcaseEventListener(new OnShowcaseEventListener() {
                    @Override
                    public void onShowcaseViewHide(ShowcaseView showcaseView) {
                        unDimView(tripNavigate_button);
                        unDimView(title);
                        unDimView(newsNavigate_button);
                        unDimView(prefNavigate_button);
                        displayShowcaseViewFour();
                    }

                    @Override
                    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

                    }

                    @Override
                    public void onShowcaseViewShow(ShowcaseView showcaseView) {
                        dimView(tripNavigate_button);
                        dimView(title);
                        dimView(newsNavigate_button);
                        dimView(prefNavigate_button);
                    }
                })
                .build();
    }


    public void displayShowcaseViewFour(){
        ViewTarget target = new ViewTarget(R.id.imbtn4,this);
        new ShowcaseView.Builder(this)
                .setTarget(target)
                .setContentTitle("Preference")
                .setContentText("Modify how you want to use Traffisible. Enabling location, news service and select a trip for news display can be found here.")
                .hideOnTouchOutside()
                .singleShot(4)
                .setStyle(R.style.MyCustomTheme)
                .setShowcaseEventListener(new OnShowcaseEventListener() {
                    @Override
                    public void onShowcaseViewHide(ShowcaseView showcaseView) {
                        unDimView(tripNavigate_button);
                        unDimView(title);
                        unDimView(newsNavigate_button);
                        unDimView(camsNavigate_button);
                    }

                    @Override
                    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

                    }

                    @Override
                    public void onShowcaseViewShow(ShowcaseView showcaseView) {
                        dimView(tripNavigate_button);
                        dimView(title);
                        dimView(newsNavigate_button);
                        dimView(camsNavigate_button);
                    }
                })
                .build();
    }


    /*
    นำ pass key ที่ได้ไปขอข้อมูลตามหัวข้อข่าวที่เราต้องการและนำข้อมูลที่ได้รับไปเก็บไว้ใน database
     */
    public void loadContent() {
        url = getURLNews(APP_ID);
        url2 = getURLCCTV(APP_ID);
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    MyDbHelper myDbHelper = new MyDbHelper(getApplicationContext());
                    newsJsonString = new String(responseBody);

                    mDb = myDbHelper.getWritableDatabase();
                    JsonParser jsonParser = new JsonParser();
                    jsonElement = jsonParser.parse(newsJsonString);
                    mDb.delete(myDbHelper.TABLE_NAME3, null, null);
                    ContentValues test = new ContentValues();
                    test.put(MyDbHelper.COL_ID3, newsJsonString);
                    mDb.insert(MyDbHelper.TABLE_NAME3, null, test);
                    mDb.close();
                    if(numberOfLoad < 1){
                        numberOfLoad ++;
                    }else{
                        progress.dismiss();
                    }
                } catch (JsonSyntaxException e) {
                    setPassKey();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
        client.get(url2, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    MyDbHelper myDbHelper = new MyDbHelper(getApplicationContext());
                    cctvJsonString = new String(responseBody);
                    mDb = myDbHelper.getWritableDatabase();
                    JsonParser jsonParser = new JsonParser();
                    jsonElement = jsonParser.parse(cctvJsonString);
                    mDb.delete(myDbHelper.TABLE_NAME4, null, null);
                    ContentValues test2 = new ContentValues();
                    test2.put(MyDbHelper.COL_ID4, cctvJsonString);
                    mDb.insert(MyDbHelper.TABLE_NAME4, null, test2);
                    mDb.close();
                    if (numberOfLoad < 1) {
                        numberOfLoad++;
                    } else {
                        progress.dismiss();
                    }
                }catch (JsonSyntaxException e){
                    setPassKey();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    //เข้ารหัส Pass Key
    private static String md5(String s) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes(), 0, s.length());
            return new BigInteger(1, digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    /*
        ก่อนที่จะขอ Traffy API ต้องมีการขอ Pass Key ก่อนซึ่งได้จากการนำ App ID ที่ได้จากการสมัครขอใช้บริการ Traffy API โดยการ
        ขอ Pass key จะต้องทำการส่ง App ID ไปให้ Traffy API
     */
    public void setPassKey() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://api.traffy.in.th/apis/getKey.php?appid=" + APP_ID, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                String key = new String(response);
                passKey = md5(APP_ID + key) + md5(KEY + key);
                loadContent();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    public String getPassKey() {
        return passKey;
    }


    public String getURLNews(String APP_ID) {
        return "http://api.traffy.in.th/apis/apitraffy.php?api=" + "getIncident" + "&key=" + passKey + "&format=" + "JSON" + "&appid=" + APP_ID;
    }

    public String getURLCCTV(String APP_ID) {
        return "http://api.traffy.in.th/apis/apitraffy.php?format=JSON&api=getCCTV&available=t&key=" + passKey + "&appid=" + APP_ID;
    }

    private void dimView(View view) {
        if (apiUtils.isCompatWithHoneycomb()) {
            view.setAlpha(0.1f);
        }
    }

    private void unDimView(View view) {
        if (apiUtils.isCompatWithHoneycomb()) {
            view.setAlpha(1f);
        }
    }


//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data){
//        if(resultCode == RESULT_OK && requestCode == 1234){
//            Intent intent = getIntent();
//            BackgroundService = Integer.parseInt(intent.getStringExtra("Background"));
//            NotiService = Integer.parseInt(intent.getStringExtra("Noti"));
//        }else
//            Toast.makeText(getApplicationContext(), "No action has been performed", Toast.LENGTH_LONG).show();
//    }
}
