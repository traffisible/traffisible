package com.example.ripzery.traffisible.maps;

import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.ripzery.traffisible.R;
import com.example.ripzery.traffisible.fragment.RecordAddFragment;
import com.example.ripzery.traffisible.database.DataLatLng;
import com.example.ripzery.traffisible.database.MyDbHelper;
import com.example.ripzery.traffisible.utils.SphericalUtil;
import com.example.ripzery.traffisible.utils.TypefaceSpan;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Nathadanai_C on 11/24/14.
 */

/*
    ให้ผู้ใช้ได้ทำการ record เส้นทางที่ผู้ใช้ต้องการ พร้อมกับบันทึกเส้นทางของผู้ใช้ลงใน database
 */
public class RecordMapFragmentActivity extends ActionBarActivity {
    private GoogleMap myMap;
    private Button mStopBtn;
    private int stopTrigger = 1;
    private ArrayList<LatLng> arr_latlng;
    private String tripname;
    private int camFlag = 1;
    private android.support.v7.app.ActionBar mActionBar;
    private PolylineOptions mLine;
    private MyDbHelper mDbHelper;
    private DataLatLng data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_mapactivity_layout);
        Intent intent = getIntent();
        tripname = intent.getStringExtra("tripname");

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setHomeAsUpIndicator(R.drawable.ic_ab_up_compat);
        setTitle(tripname);

        mDbHelper = new MyDbHelper(getApplicationContext());
        SupportMapFragment mySupportMapFragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map_canvas);
        myMap = mySupportMapFragment.getMap();
        myMap.setMyLocationEnabled(true);
        myMap.getUiSettings().setZoomGesturesEnabled(false);

        mStopBtn = (Button)findViewById(R.id.cancel_btn);
        mStopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopTrigger = 0;
                saveDataToDatabase();
                Intent intent = new Intent(RecordMapFragmentActivity.this,RecordAddFragment.class);
                setResult(RESULT_OK,intent);
                finish();
            }
        });

        if(stopTrigger == 1){
            arr_latlng = new ArrayList<LatLng>();
        }

        myMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                if (stopTrigger == 1) {
                    double Latitude = location.getLatitude();
                    double Longitude = location.getLongitude();

                    if (location.getAccuracy() < 100) {
                        final LatLng latlng = new LatLng(Latitude, Longitude);
                        if(camFlag == 1) {
                            setCameraPosition(latlng);
                            camFlag = 0;
                        }

                        arr_latlng.add(latlng);
//                        Log.d("latlng tracker", String.valueOf(latlng));
                        if(arr_latlng.size()>1){
                            mLine = new PolylineOptions().width(4).color(Color.GREEN);
                            mLine.add(arr_latlng.get(arr_latlng.size()-1),arr_latlng.get(arr_latlng.size()-2));
                            myMap.addPolyline(mLine);
                        }
                    }
                }
            }
        });
    }

    /*
        ทำการบันทึกข้อมูลของเส้นทางลง database โดยจะเก็บชื่อเส้นทาง ระยะทาง วันและเวลาที่เดินทาง
     */
    public void saveDataToDatabase(){
        double distance = 0;
        String distanceCal = "";
        for(int i=0;i<arr_latlng.size()-1;i++){
            distance += SphericalUtil.computeDistanceBetween(arr_latlng.get(i),arr_latlng.get(i+1));
        }
        distanceCal = Integer.toString((int)distance);
        Calendar rightNow = Calendar.getInstance();
        String date = rightNow.get(Calendar.DATE) + "/" + (rightNow.get(Calendar.MONTH)+1) + "/" + rightNow.get(Calendar.YEAR);
        String time = rightNow.get(Calendar.HOUR_OF_DAY) + ":" + rightNow.get(Calendar.MINUTE);
        data = new DataLatLng(tripname,arr_latlng,distanceCal,date,time);
        mDbHelper.Add_data(data);
        Toast.makeText(getApplicationContext(),"Data Added Successfully. Name of the trip is " + tripname,Toast.LENGTH_LONG).show();
        try {
            ArrayList<LatLng> fetchLatlng = mDbHelper.getLatLng(tripname);
            for(int i = 0;i<fetchLatlng.size();i++){
                String print_latlng = fetchLatlng.get(i).toString();
//                Log.d("latlng checker",print_latlng);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        distanceCal = mDbHelper.get_DistanceFromTripname(tripname);
        Toast.makeText(getApplicationContext(),"Return distance is : " + distanceCal, Toast.LENGTH_LONG).show();
        //Log.d("return distance",distanceCal);
    }

    //กำหนดให้ google map ติดตามตำแหน่งล่าสุดของผู้ใช้ไว้เสมอ
    private void setCameraPosition(LatLng Location) {
        CameraPosition camPos = new CameraPosition.Builder().target(Location).zoom(17).tilt(45).build();
        myMap.animateCamera(CameraUpdateFactory.newCameraPosition(camPos),new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                camFlag = 1;
            }

            @Override
            public void onCancel() {

            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setTitle(CharSequence title) {
        SpannableString mStringTitle = new SpannableString(title);
        mStringTitle.setSpan(new TypefaceSpan(this, "Roboto-Medium.ttf"), 0, mStringTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(mStringTitle);
    }
}
