package com.example.ripzery.traffisible.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.ripzery.traffisible.database.MyDbHelper;
import com.example.ripzery.traffisible.fragment.ReportNewsFragment;
import com.example.ripzery.traffisible.jsonclass.News;
import com.example.ripzery.traffisible.MyActivity;
import com.example.ripzery.traffisible.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by visit on 9/1/14 AD.
 */

/*
    CardAdapter for traffic news display.
 */
public class TrafficNewsCardAdapter extends RecyclerView.Adapter<TrafficNewsCardAdapter.ViewHolder> {
    private static  MyActivity myActivity = null;
    private static  ArrayList<News> listNews = null;
    private int lastPosition = -1;

    public TrafficNewsCardAdapter(MyActivity context, ArrayList<News> objects) {
        this.listNews = objects;
        this.myActivity = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvTitle;
        TextView tvDes;
        TextView tvStartTime;
        Button btnMaps;
        Button btnHide;
        CardView container;
        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
//            icon = (IconTextView) itemView.findViewById(R.id.iconify);
            tvDes = (TextView) itemView.findViewById(R.id.tvDes);
            tvStartTime = (TextView) itemView.findViewById(R.id.tvTime);
            btnMaps = (Button) itemView.findViewById(R.id.btnGoMap);
            btnMaps.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    myActivity.openMapFragment(listNews, view, getPosition());
                }
            });

            btnHide = (Button) itemView.findViewById(R.id.btnHide);
            btnHide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SQLiteDatabase mDb;
                    MyDbHelper myDbHelper = new MyDbHelper(myActivity);
                    mDb = myDbHelper.getWritableDatabase();
                    ContentValues test = new ContentValues();
                    test.put(MyDbHelper.COL_ID,listNews.get(getPosition()).getId());
                    mDb.insert(MyDbHelper.TABLE_NAME, null, test);
                    listNews.remove(getPosition());
                    TrafficNewsCardAdapter.this.notifyDataSetChanged();
                }
            });
            container = (CardView)itemView.findViewById(R.id.card_view);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.traffic_news_cardview,parent,false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Typeface tfBold = Typeface.createFromAsset(myActivity.getAssets(), "fonts/RSU_BOLD.ttf");
        Typeface tfMedium = Typeface.createFromAsset(myActivity.getAssets(), "fonts/RSU_Regular.ttf");
        Typeface tfLight = Typeface.createFromAsset(myActivity.getAssets(), "fonts/RSU_light.ttf");
        News news = listNews.get(position);
        String s = news.getTitle();
        viewHolder.tvTitle.setText(s);
        viewHolder.tvTitle.setTypeface(tfBold);

        String type = news.getType();

        s = news.getDescription();
        viewHolder.tvDes.setText(s);
        viewHolder.tvDes.setTypeface(tfMedium);
        s = news.getStarttime();
        viewHolder.tvStartTime.setText(s);
        viewHolder.tvStartTime.setTypeface(tfLight);
//        setAnimation(viewHolder.container, position);

        if (listNews.get(position).getLocation().getPoint().getLatitude() == null){
            viewHolder.btnMaps.setVisibility(View.INVISIBLE);
            Log.d("Latitude Null","Hide");
        }
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(myActivity, android.R.anim.fade_in);
            animation.setDuration(1800);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        return listNews.size();
    }

}
