package com.example.ripzery.traffisible.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ripzery on 11/20/2014.
 */
public class MyDbHelper extends SQLiteOpenHelper{
    private static final String DB_NAME = "Traffisible";
    private static final int DB_VERSION = 1;
    public static final String TABLE_NAME = "HiddenReport";
    public static final String COL_ID = "news_id";
    public static final String TABLE_NAME2 = "HiddenCCTV";
    public static final String COL_ID2 = "cctv_id";
    public static final String TABLE_NAME3 = "LatestNews";
    public static final String COL_ID3 = "news";
    public static final String TABLE_NAME4 = "LatestCCTV";
    public static final String COL_ID4 = "cctv";
    public static final String TABLE_NAME5 = "UserTrip";
    public static final String COLUMN_NAME = "_Tripname";
    public static final String COLUMN_LATLNG = "_latlng";
    public static final String COLUMN_DISTANCE = "_distance";
    public static final String COLUMN_DATE = "_date";
    public static final String COLUMN_TIME = "_time";
    private static final String DATABASE_CREATE = "CREATE TABLE "
            +TABLE_NAME5+"("
            +COLUMN_NAME+" Text primary key,"
            +COLUMN_LATLNG + " Text not null,"
            +COLUMN_DISTANCE + " Text not null,"
            +COLUMN_DATE + " Text not null,"
            +COLUMN_TIME + " Text not null"
            + ");";

    public MyDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME +" ("
                + COL_ID + " TEXT primary key);");
        db.execSQL("CREATE TABLE " + TABLE_NAME2 +" ("
                + COL_ID2 + " TEXT primary key);");
        db.execSQL("CREATE TABLE " + TABLE_NAME3 +" ("
                + COL_ID3 +" TEXT primary key);");
        db.execSQL("CREATE TABLE " + TABLE_NAME4 +" ("
                + COL_ID4 +" TEXT primary key);");
        db.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME2);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME3);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME4);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME5);
        onCreate(db);
    }

    /*
        เพิ่มเส้นทางของผู้ใช้ลงใน database ในรูปของ JSON
     */
    public void Add_data(DataLatLng data){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        //Add tripname to database
        values.put(COLUMN_NAME,data.getName());

        //Initialize JSON
        JSONArray latlng_jsonarr = new JSONArray();
        ArrayList<LatLng> latlng_arr = data.getLatlng();

        //Add latlng to database
        for(int i = 0; i<latlng_arr.size(); i++){
            JSONObject ObLatlng_json = new JSONObject();
            try {
                String lat = String.valueOf(latlng_arr.get(i).latitude);
                String lng = String.valueOf(latlng_arr.get(i).longitude);
                ObLatlng_json.put(COLUMN_LATLNG,lat+","+lng);
                latlng_jsonarr.put(ObLatlng_json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.d("date",data.get_date());
        Log.d("time",data.get_time());
        values.put(COLUMN_LATLNG,latlng_jsonarr.toString());
        values.put(COLUMN_DISTANCE,data.get_Distance());
        values.put(COLUMN_DATE,data.get_date());
        values.put(COLUMN_TIME,data.get_time());
        db.insert(TABLE_NAME5,null,values);
        db.close();
    }

    /*
        ดึงค่าของ Latitude และ Longitude ของเส้นทางที่สนใจจาก database โดยคืนค่าออกมาเป็น ArrayList<LatLng>
     */
    public ArrayList<LatLng> getLatLng(String tripname) throws JSONException{
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME5, new String[] {COLUMN_NAME, COLUMN_LATLNG, COLUMN_DISTANCE}, COLUMN_NAME + "=?",
                new String[] { tripname }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        ArrayList<LatLng> convert_latlng = new ArrayList<LatLng>();
        JSONArray LatLng_jsonarr = new JSONArray(cursor.getString(1));

        for(int i = 0; i<LatLng_jsonarr.length();i++){
            JSONObject ObLatLng_json = new JSONObject();
            ObLatLng_json = LatLng_jsonarr.getJSONObject(i);
            String[] latlng = ObLatLng_json.getString("_latlng").split(",");

            Double lat = Double.parseDouble(latlng[0]);
            Double lng = Double.parseDouble(latlng[1]);
            convert_latlng.add(new LatLng(lat, lng));
        }
        db.close();
        Log.d("LatLngSize",""+convert_latlng.size());
        return convert_latlng;
    }

    /*
        return all trips that are stored in database as an ArrayList<String>
     */
    public ArrayList<String> getListItem(){
        String selectQuery = "SELECT * FROM " + TABLE_NAME5;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<String> name_Arr = new ArrayList<String>();

        if(cursor.moveToFirst()){
            do{
                name_Arr.add(cursor.getString(0));
            }while(cursor.moveToNext());
        }
        db.close();
        return name_Arr;
    }

    /*
        Return all the Objects from class DataLatLng that are stored  in database
     */
    public ArrayList<DataLatLng> getAllRecordDataLatLng(){
        String selectQuery = "SELECT * FROM " + TABLE_NAME5;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<DataLatLng> listRecords = new ArrayList<DataLatLng>();

        if(cursor.moveToFirst()){
            do{
                DataLatLng record = new DataLatLng(cursor.getString(0),cursor.getString(2),cursor.getString(3),cursor.getString(4));
                listRecords.add(record);
            }while(cursor.moveToNext());
        }

        db.close();
        return listRecords;
    }

    /*
        Calculate the total distance of user's trip
     */
    public String get_DistanceFromTripname(String tripname){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME5, new String[] {COLUMN_NAME, COLUMN_LATLNG, COLUMN_DISTANCE}, COLUMN_NAME + "=?",
                new String[] { tripname }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        String return_distance = cursor.getString(2);
        db.close();
        return  return_distance;
    }

    public String getTimeFromTripname(String tripname){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME5, new String[] {COLUMN_NAME, COLUMN_LATLNG, COLUMN_DISTANCE, COLUMN_DATE, COLUMN_TIME}, COLUMN_NAME + "=?",
                new String[] { tripname }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        String returnTime = cursor.getString(4);
        db.close();
        return  returnTime;
    }

    public String getDateFromTripname(String tripname){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME5, new String[] {COLUMN_NAME, COLUMN_LATLNG, COLUMN_DISTANCE, COLUMN_DATE, COLUMN_TIME}, COLUMN_NAME + "=?",
                new String[] { tripname }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        String returnDate = cursor.getString(3);
        db.close();
        return  returnDate;
    }

    public void removeData(String tripname){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME5, COLUMN_NAME +"=?", new String[]{tripname});
        db.close();
    }

}
