package com.example.ripzery.traffisible.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.example.ripzery.traffisible.MyActivity;
import com.example.ripzery.traffisible.R;
import com.example.ripzery.traffisible.database.MyDbHelper;
import com.example.ripzery.traffisible.jsonclass.News;
import com.example.ripzery.traffisible.utils.SphericalUtil;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by ripzery on 11/27/2014.
 */

/*
 *  Service ในการ update location เพื่อนำไปตรวจสอบว่าใกล้กับข่าวไหนที่ดึงมาหรือไม่
 *  หากตรงก็จะแจ้งเตือน Notification ให้ผู้ใช้ได้ทราบ
 */
public class UpdateLocationService extends Service {

    LocationManager mLocationManager = null;
    GPSTracker myGPSTracker;
    private double minNewsDistance = 5000;
    long minTime = 10000;
    private float minDistance = 1;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void setMinTime(long minTime){
        this.minTime = minTime;
    }

    public long getMinTime(){
        return this.minTime;
    }

    @Override
    public void onCreate()
    {
        Toast.makeText(getApplication(), "Location service is running", Toast.LENGTH_SHORT).show();
        SharedPreferences sharedPref = getSharedPreferences("preferences",Context.MODE_PRIVATE);
        double prefMinTime = sharedPref.getInt("LocInterval",180000);
        setMinTime((long)prefMinTime);
        Log.d("PrefTime",""+prefMinTime);

        myGPSTracker = new GPSTracker();
        if (mLocationManager == null) {
            mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDistance, myGPSTracker);
    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Toast.makeText(getApplication(), "Location service is stopped", Toast.LENGTH_SHORT).show();
        if (mLocationManager != null) {
            mLocationManager.removeUpdates(myGPSTracker);
        }
    }

    private class GPSTracker implements android.location.LocationListener {
        private SQLiteDatabase mDb;
        private String jsonNews;
        private String jsonString;
        private JsonElement jsonElement;
        private ArrayList<News> listNews;
        private News nearestNews;
        private double previousDistance = 0.0;

        @Override
        public void onLocationChanged(Location location) {
            // TODO Auto-generated method stub

            if(listNews == null){
                MyDbHelper myDbHelper = new MyDbHelper(getApplicationContext());
                mDb = myDbHelper.getWritableDatabase();
                String[] allColumns = {MyDbHelper.COL_ID3};
                Cursor cursor = mDb.query(MyDbHelper.TABLE_NAME3, allColumns, null, null, null, null, null);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    jsonNews = cursor.getString(0);
                    cursor.moveToNext();
                }
                cursor.close();
                if (jsonNews != null) {
                    jsonString = new String(jsonNews);
                    Gson gson = new Gson();
                    JsonParser jsonParser = new JsonParser();
                    jsonElement = jsonParser.parse(jsonString);
                    jsonElement = jsonElement.getAsJsonObject().getAsJsonObject("info").get("news");
                    News[] news = gson.fromJson(jsonElement, News[].class);
                    listNews = new ArrayList<News>();
                    Collections.addAll(listNews, news);
                    Log.d("UpdateLocation : ", location.toString());
                }
            }

            for (News news : listNews){
                if(news.getLocation().getPoint().getLatitude() != null){
                    double latitude = Double.parseDouble(news.getLocation().getPoint().getLatitude());
                    double longitude = Double.parseDouble(news.getLocation().getPoint().getLongitude());
                    LatLng myLatLng =  new LatLng(location.getLatitude(),location.getLongitude());
                    LatLng newsLatLng = new LatLng(latitude,longitude);
                    double distance = SphericalUtil.computeDistanceBetween(myLatLng,newsLatLng);

                    if(distance < minNewsDistance){
                        if(nearestNews == null){
                            nearestNews = news;
                        }else{
                            if(distance < previousDistance){
                                nearestNews = news;
                            }
                        }
                    }
                    previousDistance = distance;
                }
            }
            if(nearestNews != null){
                showNotification(nearestNews);
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onProviderEnabled(String provider) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // TODO Auto-generated method stub

        }
    }

    private void showNotification(News news) {
        NotificationManager mNotificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        Notification notification = new Notification(R.drawable.ic_launcher,null,System.currentTimeMillis());
        Intent noti = new Intent(this, MyActivity.class);
        noti.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,noti, 0);
        notification.setLatestEventInfo(this, "ข่าวใกล้เคียง",news.getTitle()+ " : " +news.getDescription(), contentIntent);
        notification.defaults|= Notification.DEFAULT_ALL;
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        mNotificationManager.notify(1, notification);
    }
}
