package com.example.ripzery.traffisible.maps;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.ripzery.traffisible.R;
import com.example.ripzery.traffisible.utils.TypefaceSpan;
import com.example.ripzery.traffisible.jsonclass.News;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

/*
    เป็นหน้าแสดงตำแหน่งของข่าวที่เลือกดู โดยจะแสดง marker ลงบน Google Map
 */


public class MapsActivity extends ActionBarActivity {
    // TODO: Rename parameter arguments, choose names that match
    private final LatLngBounds Thailand = new LatLngBounds(new LatLng(5.371270, 97.859916), new LatLng(19.680066, 104.957083));
    private String mName, mDescription, mTitle, mType, mLocationType;
    private String mLatitude, mLongitude;

    private android.support.v7.app.ActionBar mActionBar;
    private GoogleMap mMap;
    private News news;

    public MapsActivity() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_maps);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setHomeAsUpIndicator(R.drawable.ic_ab_up_compat);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mType = bundle.getString("type");
            mTitle = bundle.getString("title");
            mDescription = bundle.getString("des");
            mLocationType = bundle.getString("location_type");
            if (mLocationType.equals("point")){
                mName = bundle.getString("name");
                mLatitude = bundle.getString("lat");
                mLongitude = bundle.getString("lon");
            }else{
                mName = bundle.getString("startpoint_name");
                mLatitude = bundle.getString("startpoint_lat");
                mLongitude = bundle.getString("startpoint_lon");
            }



        }

        setTitle(" TRAFFISIBLE");
        setSubTitle("     "+mName);

        TextView tvRoad = (TextView)findViewById(R.id.tvRoad);
        tvRoad.setText("ตำแหน่ง : " + mName + "\n" + "เหตุการณ์ : " + mDescription);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/RSU_Regular.ttf");
        tvRoad.setTypeface(tf);
        setUpMapIfNeeded();
        Log.d("latitude",""+mLatitude);
        final LatLng latlng = new LatLng(Double.parseDouble(mLatitude), Double.parseDouble(mLongitude));
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
//                setUpMap();
                setCameraPosition(latlng);
                MarkerOptions marker;

                String extend = "";
                if (mTitle.equals("การก่อสร้าง")) {
                    extend = "มี";
                }
                marker = new MarkerOptions().position(latlng).title("ตำแหน่ง : " + mName + " " + extend + mTitle).icon(getIcon());
                mMap.addMarker(marker);


            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    แสดง icon ตามเหตุการณ์ชนิดต่างๆลงบน Google Map
     */
    public BitmapDescriptor getIcon() {
        BitmapDescriptor bitmap;
        if (mType.equals("สภาพอากาศ")) {
            bitmap = mTitle.equals("ฝนตก") ? BitmapDescriptorFactory.fromAsset("marker-icons/rainy.png") : BitmapDescriptorFactory.fromAsset("marker-icons/flood.png");
        } else if (mType.equals("เหตุฉุกเฉิน")) {
            bitmap = BitmapDescriptorFactory.fromAsset("marker-icons/caution.png");
        } else if (mType.equals("การก่อสร้าง")) {
            bitmap = BitmapDescriptorFactory.fromAsset("marker-icons/bulldozer.png");
        } else if (mType.equals("การจราจร")) {
            bitmap = BitmapDescriptorFactory.fromAsset("marker-icons/flagman.png");
        } else {
            bitmap = BitmapDescriptorFactory.fromAsset("marker-icons/repair.png");
        }

        return bitmap;
    }

    private void setCameraPosition(LatLng Location) {
        CameraPosition camPos = new CameraPosition.Builder().target(Location).zoom(17).tilt(45).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camPos));
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mMap.clear();
    }


    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
//                setUpMap();
            }
        }
    }

    public void setSubTitle(String subTitle) {
        SpannableString mStringSubTitle = new SpannableString(subTitle);
        mStringSubTitle.setSpan(new TypefaceSpan(this, "Roboto-Light.ttf"), 0, mStringSubTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setSubtitle(mStringSubTitle);
    }

    /*
        set ชื่อข่าวที่เราได้เลือกเข้ามาที่Action Bar
     */
    @Override
    public void setTitle(CharSequence title) {
        SpannableString mStringTitle = new SpannableString(title);
        mStringTitle.setSpan(new TypefaceSpan(this, "Roboto-Medium.ttf"), 0, mStringTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(mStringTitle);
    }

    public void setUpMap() {
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(Thailand, 3));
    }

}
