package com.example.ripzery.traffisible.service;

import android.app.Activity;
import android.app.IntentService;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.ResultReceiver;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.ripzery.traffisible.R;
import com.example.ripzery.traffisible.database.MyDbHelper;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.MalformedJsonException;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.apache.http.Header;

import java.io.IOException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by ripzery on 11/22/2014.
 */

/*
    เป็น service ในการ update ข่าวเป็นช่วงเวลาที่ผู้ใช้เลือกและเก็บไว้ใน database เพื่อให้เวลาเปิดข่าวขึ้นมาไม่เสียเวลาในการโหลด
 */
public class UpdateNewsService extends IntentService{
    private static final String APP_ID = "61d787a9";
    private static final String KEY = "jADjas9PXU";
    public static final String ACTION = "traffisible";
    private JsonElement newsJsonElement, cctvJsonElement;
    private String newsJsonString, cctvJsonString;
    private String url, url2;
    private String passKey = "";
    private SQLiteDatabase mDb;
    private JsonElement jsonElement;

    public UpdateNewsService(){
        super("test service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.i("UpdateNewsService", "Service is running");
        if (!isPassKeySet()){
            setPassKey();
        }
    }

    public void loadContent(){
        url = getURLNews(APP_ID);
        url2 = getURLCCTV(APP_ID);
        OkHttpClient client = new OkHttpClient();
        Log.d("URL",url);
        Request request = new Request.Builder().url(url).build();
        Request request2 = new Request.Builder().url(url2).build();
        try {
            Response newsResponse = client.newCall(request).execute();
            Response cctvResponse = client.newCall(request2).execute();

            String newsOutput = newsResponse.body().string();
            Log.d("Success News Fetch ","complete");

            String cctvOutput = cctvResponse.body().string();
            Log.d("Success CCTV Fetch ","complete");

            MyDbHelper myDbHelper = new MyDbHelper(getApplicationContext());

            newsJsonString = new String(newsOutput);
            cctvJsonString = new String(cctvOutput);

            mDb = myDbHelper.getWritableDatabase();

            JsonParser jsonParser = new JsonParser();
            jsonElement = jsonParser.parse(newsJsonString);
            mDb.delete(myDbHelper.TABLE_NAME3,null,null);
            ContentValues test = new ContentValues();
            test.put(MyDbHelper.COL_ID3,newsJsonString);
            mDb.insert(MyDbHelper.TABLE_NAME3, null, test);

            mDb.delete(myDbHelper.TABLE_NAME4,null,null);
            ContentValues test2 = new ContentValues();
            test2.put(MyDbHelper.COL_ID4,cctvJsonString);
            mDb.insert(MyDbHelper.TABLE_NAME4, null, test2);
            mDb.close();

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this).setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle("Traffisible")
                    .setContentText("Fresh news is ready !");

            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(123,mBuilder.build());
        }catch (JsonSyntaxException e){
            Log.d("Exception!",e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static String md5(String s) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes(), 0, s.length());
            return new BigInteger(1, digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void setPassKey() {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("http://api.traffy.in.th/apis/getKey.php?appid=" + APP_ID).build();
        try {
            Response response = client.newCall(request).execute();
            String output = response.body().string();
            Log.d("Success BG Fetch! >>>",output);
            passKey = md5(APP_ID + output) + md5(KEY + output);
            loadContent();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getPassKey() {
        return passKey;
    }
    

    public String getURLNews(String APP_ID) {
        return "http://api.traffy.in.th/apis/apitraffy.php?api=" + "getIncident" + "&key=" + passKey + "&format=" + "JSON" + "&appid=" + APP_ID;
    }

    public String getURLCCTV(String APP_ID) {
        return "http://api.traffy.in.th/apis/apitraffy.php?format=JSON&api=getCCTV&available=t&key=" + passKey + "&appid=" + APP_ID;
    }
    
    public boolean isPassKeySet() {
        return !passKey.equals("");
    }
}
