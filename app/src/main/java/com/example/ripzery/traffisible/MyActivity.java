package com.example.ripzery.traffisible;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ripzery.traffisible.database.MyDbHelper;
import com.example.ripzery.traffisible.fragment.CCTVFragment;
import com.example.ripzery.traffisible.fragment.PreferFragment;
import com.example.ripzery.traffisible.fragment.RecordAddFragment;
import com.example.ripzery.traffisible.fragment.ReportNewsFragment;
import com.example.ripzery.traffisible.jsonclass.CCTV;
import com.example.ripzery.traffisible.jsonclass.News;
import com.example.ripzery.traffisible.maps.MapsActivity;
import com.example.ripzery.traffisible.maps.MapsCCTVActivity;
import com.example.ripzery.traffisible.service.MyAlarmReceiver;
import com.example.ripzery.traffisible.service.UpdateNewsService;
import com.example.ripzery.traffisible.utils.TypefaceSpan;
import com.google.gson.JsonParser;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;


/*
    เป็น Activity หลักที่รวบรวม fragment ของหน้าต่างๆเอาไว้ด้วยกัน
 */

public class MyActivity extends ActionBarActivity {
    public Fragment reportFrag, cctvFrag, recordFrag, preferFlag;
    private MapsActivity mapsActivity;
    private MapsCCTVActivity mapsCCTVActivity;
    private ActionBarDrawerToggle mDrawerToggle;
    private String[] mDrawerString;
    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private android.support.v7.app.ActionBar mActionBar;
    private Fragment newFragment, oldFragment;
    private int intervalMillis = 1800000;
    private MyAlarmReceiver myAlarmReceiver;
    private SQLiteDatabase mDb;

    private static String md5(String s) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes(), 0, s.length());
            return new BigInteger(1, digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void cancelAlarm() {
            Intent intent = new Intent(getApplicationContext(), MyAlarmReceiver.class);
            final PendingIntent pIntent = PendingIntent.getBroadcast(this, MyAlarmReceiver.REQUEST_CODE,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
            alarm.cancel(pIntent);
            Toast.makeText(this,"Update news service is stopped",Toast.LENGTH_SHORT).show();
    }

    public void scheduleAlarm() {
        // Construct an intent that will execute the AlarmReceiver
        Intent intent = new Intent(getApplicationContext(), MyAlarmReceiver.class);
        // Create a PendingIntent to be triggered when the alarm goes off
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, MyAlarmReceiver.REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        // Setup periodic alarm every 30 seconds
        long firstMillis = System.currentTimeMillis(); // first run of alarm is immediate
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis, getIntervalMillis(), pIntent);
        Toast.makeText(this,"Update news service is running",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
//        scheduleAlarm();

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);

        mActionBar = getSupportActionBar();
        mActionBar.setShowHideAnimationEnabled(true);
        mActionBar.setElevation(5);
        setTitle(" TRAFFISIBLE");
        setSubTitle("     คลิกแถบด้านซ้ายเพื่อเลือกเมนูข่าว");
        mActionBar.setHomeButtonEnabled(true);
//        mActionBar.setBackgroundDrawable(null);
        mActionBar.setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        mDrawerString = getResources().getStringArray(R.array.drawer_array);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, mDrawerString));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
//        mDrawerList.setItemChecked(0,true);
        mDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout,R.string.drawer_open,R.string.drawer_close);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        Intent intent = getIntent();
        int selectItem = intent.getIntExtra("selectItem",0);
        selectItem(selectItem);
    }

    public void openMapFragment(ArrayList<News> listNews, View view, int i) {

        News news = listNews.get(i);
        Bundle bundle = new Bundle();
        News.Location location = news.getLocation();
        bundle.putString("location_type",location.getType());
        if (location.getType().equals("point")) {
            Log.d("point : ",""+i);

            News.Location.Point point = location.getPoint();
            bundle.putString("type", news.getType());
            bundle.putString("title", news.getTitle());
            bundle.putString("des", news.getDescription());
            bundle.putString("name", point.getName());
            bundle.putString("lat", point.getLatitude());
            bundle.putString("lon", point.getLongitude());

        } else if (location.getType().equals("line")) {
            Log.d("line : ",""+i);

            News.Location.Road road = location.getRoad();
            News.Location.Startpoint startPoint = location.getStartPoint();
            News.Location.Endpoint endPoint = location.getEndPoint();

            bundle.putString("type", news.getType());
            bundle.putString("title", news.getTitle());
            bundle.putString("des", news.getDescription());
            bundle.putString("road", road.getName());
            bundle.putString("startpoint_name", startPoint.getName());
            bundle.putString("startpoint_lat", startPoint.getLatitude());
            bundle.putString("startpoint_lon", startPoint.getLongitude());
            bundle.putString("endpoint_name", endPoint.getName());
            bundle.putString("endpoint_lat", endPoint.getLatitude());
            bundle.putString("endpoint_lon", endPoint.getLongitude());
        }

        mapsActivity = new MapsActivity();
        Intent openMapIntent = new Intent(this,MapsActivity.class);
        openMapIntent.putExtras(bundle);
        if(!location.getType().equals(" point"))
             startActivity(openMapIntent);
    }

    public void openCCTVFragment(ArrayList<CCTV> listCCTV, View view, int i) {
        CCTV cctv = listCCTV.get(i);
        CCTV.Point point = cctv.getPoint();
        Bundle bundle = new Bundle();
        Log.d("Position", "Lat : " + point.getLat() + " Long : " + point.getLng());
        bundle.putString("name", cctv.getName());
        bundle.putString("lat", point.getLat());
        bundle.putString("lng", point.getLng());
        bundle.putString("img", cctv.getList());

        Intent openMapIntent = new Intent(this, MapsCCTVActivity.class);
        openMapIntent.putExtras(bundle);
        startActivity(openMapIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            MyDbHelper myDbHelper = new MyDbHelper(getApplicationContext());
            mDb = myDbHelper.getWritableDatabase();
            mDb.delete(MyDbHelper.TABLE_NAME,null,null);
            ((ReportNewsFragment)reportFrag).showNewsWithOption(ReportNewsFragment.mode);
            return true;
        }
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void selectItem(final int position) {
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out);
        oldFragment = newFragment;
        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerList);
        Log.d("postion",""+position);
        switch (position) {
            case 0:
                if (reportFrag == null) {
                    reportFrag = new ReportNewsFragment();
                    newFragment = reportFrag;
                } else {
                    newFragment = reportFrag;
                }
                setSubTitle("    รายงานข่าวจราจร - ล่าสุด");
                break;
            case 1:
                if (cctvFrag == null) {
                    cctvFrag = new CCTVFragment();
                    newFragment = cctvFrag;
                } else {
                    newFragment = cctvFrag;
                }
                setSubTitle("     ภาพถ่าย CCTV - ล่าสุด");
                break;
            case 2:
                if(recordFrag == null){
                    recordFrag = new RecordAddFragment();
                    newFragment = recordFrag;
                }else{
                    newFragment = recordFrag;
                }
                setSubTitle("     เพิ่มเส้นทาง");
                break;
            case 3:
                if(preferFlag == null){
                    preferFlag = new PreferFragment();
                    newFragment = preferFlag;
                }
                else{
                    newFragment = preferFlag;
                }
                setSubTitle("     Preference");
                break;
            case 4:
//                int pid = android.os.Process.myPid();
//                android.os.Process.killProcess(pid);
                finish();
                System.exit(0);
                break;
        }
        if (position < 4) {
            if (newFragment.isAdded()) {
                transaction.hide(oldFragment);
                transaction.show(newFragment);
            } else {
                if (oldFragment != null)
                    transaction.hide(oldFragment);
                transaction.add(R.id.content_layout, newFragment);
            }
//            transaction.replace(R.id.content_layout, newFragment);
            transaction.commit();
        }
    }

    public void setSubTitle(String subTitle) {
        SpannableString mStringSubTitle = new SpannableString(subTitle);
        mStringSubTitle.setSpan(new TypefaceSpan(this, "Roboto-Light.ttf"), 0, mStringSubTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setSubtitle(mStringSubTitle);
    }

    @Override
    public void setTitle(CharSequence title) {
        SpannableString mStringTitle = new SpannableString(title);
        mStringTitle.setSpan(new TypefaceSpan(this, "Roboto-Medium.ttf"), 0, mStringTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mActionBar.setTitle(mStringTitle);
    }

    public int getIntervalMillis() {
        return intervalMillis;
    }

    public void setIntervalMillis(int intervalMillis) {
        this.intervalMillis = intervalMillis;
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }
}
