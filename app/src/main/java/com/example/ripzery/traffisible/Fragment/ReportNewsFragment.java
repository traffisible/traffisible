package com.example.ripzery.traffisible.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.ripzery.traffisible.jsonclass.News;
import com.example.ripzery.traffisible.MyActivity;
import com.example.ripzery.traffisible.R;
import com.example.ripzery.traffisible.adapter.TrafficNewsCardAdapter;
import com.example.ripzery.traffisible.service.UpdateNewsService;
import com.example.ripzery.traffisible.utils.SphericalUtil;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;

import com.example.ripzery.traffisible.database.MyDbHelper;

public class ReportNewsFragment extends Fragment{

    public static int SHOW_ALL_NEWS = 7777;
    public static int SHOW_DEPEND_RECORD = 9999;
    public static int mode = 7777;
    private JsonElement jsonElement;
    private String jsonString;
    private String url;
    private String passKey = "";
    private ArrayList<News> listNews, closeNews;
    private MyActivity myActivity;
    private View mRootView;
    private ProgressDialog progress;
    private SQLiteDatabase mDb;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<LatLng> recordLatLngs;
    private MyDbHelper myDbHelper;
    private Gson gson;

//    private ButtonFlat btnStart;
    private String jsonNews;
    public ReportNewsFragment() {

    }

    @Override
    public void onAttach(Activity activity) {

        super.onAttach(activity);
        myActivity = (MyActivity) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_traffic_news, container, false);
        if (savedInstanceState == null) {
            this.mRootView = view;
            this.myActivity = (MyActivity) getActivity();
//            btnStart = (ButtonFlat)mRootView.findViewById(R.id.buttonStart);
//            btnStart.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    myActivity.cancelAlarm();
//                }
//            });
            myDbHelper = new MyDbHelper(myActivity);
            mDb = myDbHelper.getWritableDatabase();
            SharedPreferences sharedPref = myActivity.getSharedPreferences("preferences", Context.MODE_PRIVATE);
            Boolean isAllNews = sharedPref.getBoolean("isAllNews",true);
            mode = isAllNews ? this.SHOW_ALL_NEWS : this.SHOW_DEPEND_RECORD;
            try {
                if(!isAllNews){
                    String tripname = sharedPref.getString("tripname","notfound");
                    if(!tripname.equals("notfound")){
                        setRecordLatLngs(myDbHelper.getLatLng(tripname));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            loadContent();
            Log.d("Saved", "null");
        }

        return view;
    }

    public void loadContent() {
        String[] allColumns =  {MyDbHelper.COL_ID3};
        mDb = myDbHelper.getReadableDatabase();
        Cursor cursor = mDb.query(MyDbHelper.TABLE_NAME3, allColumns,null,null,null,null,null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            jsonNews = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();

        if(jsonNews != null) {
            jsonString = new String(jsonNews);
            gson = new Gson();
            JsonParser jsonParser = new JsonParser();
            jsonElement = jsonParser.parse(jsonString);
            jsonElement = jsonElement.getAsJsonObject().getAsJsonObject("info").get("news");

            News[] news = gson.fromJson(jsonElement, News[].class);
            Log.d("News Size", "" + news.length);
            listNews = new ArrayList<News>();
            Collections.addAll(listNews, news);

            mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
            mRecyclerView.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(myActivity);
            mRecyclerView.setLayoutManager(mLayoutManager);

            showNewsWithOption(mode);



//            ArrayList<String> allDismissedID = getAllDismissedNews();
//            for(int i = listNews.size()-1 ; i>=0 ; i --){
//                if(allDismissedID.contains(listNews.get(i).getId())){
//                    listNews.remove(i);
//                }
//            }
//
//            mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
//            mRecyclerView.setHasFixedSize(true);
//            mLayoutManager = new LinearLayoutManager(myActivity);
//            mRecyclerView.setLayoutManager(mLayoutManager);
//            mAdapter = new TrafficNewsCardAdapter(myActivity, listNews);
//            mRecyclerView.setAdapter(mAdapter);
//

//
//            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        }
    }

    private ArrayList<String> getAllDismissedNews(){
        ArrayList<String> listDismissNews = new ArrayList<String>();
        String[] allColumns =  {MyDbHelper.COL_ID};

        Cursor cursor = mDb.query(MyDbHelper.TABLE_NAME, allColumns,null,null,null,null,null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            String NewsID = cursor.getString(0);
            listDismissNews.add(NewsID);
            cursor.moveToNext();
        }
        cursor.close();
        return listDismissNews;
    }

    public void showNewsWithOption(int option) {
        News[] news = gson.fromJson(jsonElement, News[].class);
        listNews = new ArrayList<News>();
        Collections.addAll(listNews, news);

        if (option == this.SHOW_ALL_NEWS) {
            ArrayList<String> allDismissedID = getAllDismissedNews();
            for (int i = listNews.size() - 1; i >= 0; i--) {
                if (allDismissedID.contains(listNews.get(i).getId())) {
                    listNews.remove(i);
                }
            }
        }else if(option == this.SHOW_DEPEND_RECORD){
            closeNews = new ArrayList<News>();
            for(LatLng latLng : recordLatLngs){
                for(int i = listNews.size()-1 ; i >= 0 ; i--){
                    if(listNews.get(i).getLocation().getPoint().getLatitude() != null){
                        LatLng newsLatLng = new LatLng(Double.parseDouble(listNews.get(i).getLocation().getPoint().getLatitude()),Double.parseDouble(listNews.get(i).getLocation().getPoint().getLongitude()));
                        double distance = SphericalUtil.computeDistanceBetween(latLng, newsLatLng);
                        if(distance < 5000){ // that latlng is near the news location
                            // do nothing because it's already in the list
                            Log.d("keep ",listNews.get(i).getDescription() + "distance : " + distance);
                            if( !closeNews.contains(listNews.get(i)) )
                                closeNews.add(listNews.get(i));
                        }
                    }
                }
            }
            listNews.clear();
            listNews = closeNews;
        }
        mAdapter = new TrafficNewsCardAdapter(myActivity, listNews);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDb.close();
    }

    public void setRecordLatLngs(ArrayList<LatLng> listLatLng){
        recordLatLngs = listLatLng;
    }

    public String getURL(String APP_ID) {
        return "http://api.traffy.in.th/apis/apitraffy.php?api=" + "getIncident" + "&key=" + passKey + "&format=" + "JSON" + "&appid=" + APP_ID;
    }
}
