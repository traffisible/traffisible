package com.example.ripzery.traffisible.fragment;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.example.ripzery.traffisible.adapter.CCTVCardAdapter;
import com.example.ripzery.traffisible.adapter.TrafficNewsCardAdapter;
import com.example.ripzery.traffisible.database.MyDbHelper;
import com.example.ripzery.traffisible.jsonclass.CCTV;
import com.example.ripzery.traffisible.MyActivity;
import com.example.ripzery.traffisible.R;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

/*
    Open the CCTV Fragment which will show the list of CCTV image fetched from Traffy API
 */
public class CCTVFragment extends Fragment {
    private final ArrayList<CCTV> listCCTV = new ArrayList<CCTV>();
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private String passKey = "", appId = "";
    // TODO: Rename and change types of parameters
    private View mRootView;
    private MyActivity myActivity;
    private OnFragmentInteractionListener mListener;
    private SQLiteDatabase mDb;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private String jsonCCTV;
    public CCTVFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cctvimage, container, false);
        mRootView = view;
        MyDbHelper MyDbHelper = new MyDbHelper(myActivity);
        mDb = MyDbHelper.getWritableDatabase();
        loadContent();
        return view;
    }

    /*
        Fetch CCTV data from database and display in CardView
     */
    public void loadContent() {
        String[] allColumns =  {MyDbHelper.COL_ID4};
        Cursor cursor = mDb.query(MyDbHelper.TABLE_NAME4, allColumns,null,null,null,null,null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            jsonCCTV = cursor.getString(0);
            cursor.moveToNext();
        }
        cursor.close();

        if(jsonCCTV != null){
            String json = new String(jsonCCTV);
            Gson gson = new Gson();
            JsonParser parser = new JsonParser();
            JsonElement jsonElement = parser.parse(json);
            JsonObject jsonObject = jsonElement.getAsJsonArray().get(0).getAsJsonObject();
            Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.entrySet();

            for (Map.Entry<String, JsonElement> entry : entrySet) {
                jsonElement = jsonObject.get(entry.getKey());
                CCTV cctv = gson.fromJson(jsonElement, CCTV.class);
                cctv.setId(entry.getKey());
                listCCTV.add(cctv);
            }
            //Remove dismissed cctv from the list
            //                    ArrayList<String> allDismissedList = getAllDismissedCCTV();
            //                    for(int i = listCCTV.size()-1 ;i>=0;i--){
            //                        if(allDismissedList.contains(listCCTV.get(i).getId())){
            //
            //                            listCCTV.remove(i);
            //                        }
            //                    }
            mRecyclerView = (RecyclerView)mRootView.findViewById(R.id.recycler_view2);
            mRecyclerView.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(myActivity);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mAdapter = new CCTVCardAdapter(myActivity,listCCTV);

            final CCTVCardAdapter adapter = new CCTVCardAdapter(myActivity, listCCTV);
            mRecyclerView.setAdapter(adapter);
            mRecyclerView.setVisibility(View.VISIBLE);

            mDb.close();
        }
    }


    private ArrayList<String> getAllDismissedCCTV(){
        ArrayList<String> listDismissCCTV = new ArrayList<String>();
        String[] allColumns =  {MyDbHelper.COL_ID};
        Cursor cursor = mDb.query(MyDbHelper.TABLE_NAME, allColumns,null,null,null,null,null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            String cctvID = cursor.getString(0);
            listDismissCCTV.add(cctvID);
            cursor.moveToNext();
        }
        cursor.close();
//        mDb.close();
        return listDismissCCTV;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        myActivity = (MyActivity) activity;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
