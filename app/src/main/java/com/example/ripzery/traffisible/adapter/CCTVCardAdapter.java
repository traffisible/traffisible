package com.example.ripzery.traffisible.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.ripzery.traffisible.jsonclass.CCTV;
import com.example.ripzery.traffisible.MyActivity;
import com.example.ripzery.traffisible.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by visit on 9/7/14 AD.
 */

/*
    Adapter of CardView which will use inside RecycleView.
 */
public class CCTVCardAdapter extends RecyclerView.Adapter<CCTVCardAdapter.ViewHolder>{
    private static MyActivity myActivity = null;
    private static ArrayList<CCTV> listCCTV = null;
    private LayoutInflater inflater;

    public CCTVCardAdapter(MyActivity context, ArrayList<CCTV> objects) {
        this.listCCTV = objects;
        this.myActivity = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cctv_cardview,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {
        Typeface tfBold = Typeface.createFromAsset(myActivity.getAssets(), "fonts/Roboto-Bold.ttf");
        Typeface tfMedium = Typeface.createFromAsset(myActivity.getAssets(), "fonts/Roboto-Medium.ttf");
        Typeface tfLight = Typeface.createFromAsset(myActivity.getAssets(), "fonts/Roboto-Light.ttf");
        Typeface tfLightItalic = Typeface.createFromAsset(myActivity.getAssets(), "fonts/Roboto-LightItalic.ttf");

        CCTV cctv = listCCTV.get(position);
        String s = cctv.getName();
//        Log.d("test", s);
        viewHolder.tvCCTVEngName.setText(s);
        viewHolder.tvCCTVEngName.setTypeface(tfBold);

        s = cctv.getName_th();
        viewHolder.tvCCTVThaiName.setText(s);
        viewHolder.tvCCTVThaiName.setTypeface(tfMedium);
        s = cctv.getLastupdate();
        viewHolder.tvCCTVTime.setText(s);
        viewHolder.tvCCTVTime.setTypeface(tfLight);
        Picasso.with(myActivity).load(listCCTV.get(position).getUrl()).into(viewHolder.ivCCTV);
    }

    @Override
    public int getItemCount() {
        return listCCTV.size();
    }

    /*
        Use for RecycleView when user scroll back upwards
     */
    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvCCTVEngName;
        TextView tvCCTVThaiName;
        TextView tvCCTVTime;
        ImageView ivCCTV;
        public ViewHolder(View view){
            super(view);
            tvCCTVEngName = (TextView) view.findViewById(R.id.tvCCTVEngName);
            tvCCTVThaiName = (TextView) view.findViewById(R.id.tvCCTVThaiName);
            tvCCTVTime = (TextView) view.findViewById(R.id.tvCCTVTime);
            ivCCTV = (ImageView) view.findViewById(R.id.ivCCTV);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myActivity.openCCTVFragment(listCCTV, view, getPosition());
                }
            });
        }
    }
}
