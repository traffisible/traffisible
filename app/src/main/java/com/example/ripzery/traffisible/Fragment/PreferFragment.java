package com.example.ripzery.traffisible.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.ripzery.traffisible.MyActivity;
import com.example.ripzery.traffisible.R;
import com.example.ripzery.traffisible.database.DataLatLng;
import com.example.ripzery.traffisible.database.MyDbHelper;
import com.example.ripzery.traffisible.fragment.ReportNewsFragment;
import com.example.ripzery.traffisible.service.UpdateLocationService;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Nathadanai_C on 11/27/14.
 */

/*
    Fragment handler for preference. Allow user to modify usage of the application including background service, news update service
    and select a specific trip for news visualization
 */
public class PreferFragment extends Fragment {

    private Spinner mSpinner;
    private ToggleButton toggleBtn_bgService;
    private ToggleButton toggleBtn_notiService;
    private RadioButton mAllNews, mSpecificNews;
    private RadioGroup mDisplayRadioGr,mLocationUpdateGr,mNewsUpdateGr;
    private LinearLayout mLocUpdateWrapper,mNewsUpdateWrapper, mSelectTripWrapper;
    private Button saveBtn;
    private boolean isLocUpdateService = false;
    private boolean isNewsUpdateService = false;
    private boolean isAllNews = true;
    public int locUpdateInterval = 0, newsUpdateInterval = 0;
    public ArrayList<String> TripnameFromDb;
    public String SpinnerValue = "";
    public int SpinnerPosition = 0,spinPosition = 0;
    private MyDbHelper myDbHelper;
    private View mRootView;
    private MyActivity myActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        myActivity = (MyActivity) activity;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myDbHelper = new MyDbHelper(myActivity);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.preference_layout, container, false);
        mRootView = view;

        //Initial variable

        mDisplayRadioGr = (RadioGroup) mRootView.findViewById(R.id.shownews_rdgr);
        mLocationUpdateGr = (RadioGroup) mRootView.findViewById(R.id.locUpdate_rdgr);
        mNewsUpdateGr = (RadioGroup) mRootView.findViewById(R.id.news_rdgr);
        mLocUpdateWrapper = (LinearLayout) mRootView.findViewById(R.id.location_interval_wraper);
        mNewsUpdateWrapper = (LinearLayout) mRootView.findViewById(R.id.newsupdate_interval_wrapper);
        mSelectTripWrapper = (LinearLayout) mRootView.findViewById(R.id.select_trip_wrapper);
        mAllNews = (RadioButton) mRootView.findViewById(R.id.allnews_radio);
        mSpecificNews = (RadioButton) mRootView.findViewById(R.id.selecttrip_radio);
        mSpinner = (Spinner) mRootView.findViewById(R.id.spinner);

        toggleBtn_bgService = (ToggleButton) mRootView.findViewById(R.id.toggle_location);
        toggleBtn_notiService = (ToggleButton) mRootView.findViewById(R.id.toggle_newsupdate);
        saveBtn = (Button) mRootView.findViewById(R.id.prefer_save_button);

        setPreference();
        addListener();


        return view;
    }

    public void setPreference(){
        SharedPreferences sharedPref = myActivity.getSharedPreferences("preferences", Context.MODE_PRIVATE);

        TripnameFromDb = new ArrayList<String>();
        TripnameFromDb = myDbHelper.getListItem();

        if(TripnameFromDb.size() > 0){
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(myActivity,android.R.layout.simple_list_item_1,TripnameFromDb);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinner.setAdapter(adapter);
            mSpecificNews.setEnabled(true);
        }else{
            mSpecificNews.setEnabled(false);
        }

        isLocUpdateService = sharedPref.getBoolean("isLocUpdate",false);
        isNewsUpdateService = sharedPref.getBoolean("isNewsUpdate", false);
        if(isLocUpdateService){
            locUpdateInterval = sharedPref.getInt("LocInterval",180000);
        }
        if(isNewsUpdateService){
            newsUpdateInterval = sharedPref.getInt("NewsInterval",1800000);
        }
        isAllNews = sharedPref.getBoolean("isAllNews",true);

        if(!isAllNews){
            //SpinnerValue = sharedPref.getString("tripname","");
            spinPosition = sharedPref.getInt("spinPos",0);
        }

        //Location Service Elements
        if(isLocUpdateService){
            toggleBtn_bgService.setChecked(true);
            mLocUpdateWrapper.setVisibility(View.VISIBLE);
            if(locUpdateInterval == 180000)
                mLocationUpdateGr.check(R.id.preferLoc_rd1);
            else if (locUpdateInterval == 300000)
                mLocationUpdateGr.check(R.id.preferLoc_rd2);
            else
                mLocationUpdateGr.check(R.id.preferLoc_rd3);

        }else{
            toggleBtn_bgService.setChecked(false);
            mLocUpdateWrapper.setVisibility(View.GONE);
        }

        //News Update Elements
        if(isNewsUpdateService){
            toggleBtn_notiService.setChecked(true);
            mNewsUpdateWrapper.setVisibility(View.VISIBLE);
            if(newsUpdateInterval == 1800000){
                mNewsUpdateGr.check(R.id.preferNews_rd1);
            }else if(newsUpdateInterval == 3600000){
                mNewsUpdateGr.check(R.id.preferNews_rd2);
            }else
                mNewsUpdateGr.check(R.id.preferNews_rd3);

        }else{
            toggleBtn_notiService.setChecked(false);
            mNewsUpdateWrapper.setVisibility(View.GONE);
        }

        //Display option elements
        if(!isAllNews){
            mSpecificNews.setChecked(true);
            mSelectTripWrapper.setVisibility(View.VISIBLE);
            mSpinner.setSelection(spinPosition);
        }else{
            mAllNews.setChecked(true);
            mSelectTripWrapper.setVisibility(View.GONE);
        }
    }

    public void addListener(){

        //Spinner that will show all the user trip name from database and allow user to select on user desired
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SpinnerValue = mSpinner.getSelectedItem().toString();
                SpinnerPosition = mSpinner.getSelectedItemPosition();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //Set how often do you want Traffisible to compare your current location with the news location
        mLocationUpdateGr.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(i == R.id.preferLoc_rd1){
                    locUpdateInterval = 180000;
                }else if(i == R.id.preferLoc_rd2){
                    locUpdateInterval = 300000;
                }else
                    locUpdateInterval = 600000;
            }
        });

        //Set how often do you want Traffisble to fetch news update from Traffy API
        mNewsUpdateGr.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(i == R.id.preferNews_rd1){
                    newsUpdateInterval = 1800000;
                }else if(i == R.id.preferNews_rd2){
                    newsUpdateInterval = 3600000;
                }else
                    newsUpdateInterval = 10800000;
            }
        });

        mDisplayRadioGr.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(i == R.id.selecttrip_radio){
                    mSelectTripWrapper.setVisibility(View.VISIBLE);
                    isAllNews = false;
                }else {
                    mSelectTripWrapper.setVisibility(View.GONE);
                    isAllNews = true;
                }
            }
        });

        //Enable or disable location update service
        toggleBtn_bgService.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!isLocUpdateService) {
                    mLocUpdateWrapper.setVisibility(View.VISIBLE);
                    isLocUpdateService = true;
                }
                else{
                    mLocUpdateWrapper.setVisibility(View.GONE);
                    isLocUpdateService = false;
                }
            }
        });

        //Enable or disable news update service
        toggleBtn_notiService.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //If check_noti = 1 means notification service is 'enable'
                if (!isNewsUpdateService) {
                    mNewsUpdateWrapper.setVisibility(View.VISIBLE);
                    isNewsUpdateService = true;
                } else{
                    mNewsUpdateWrapper.setVisibility(View.GONE);
                    isNewsUpdateService = false;
                }

            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //myDbHelper.AddPreference(check_bg,check_noti);
                SharedPreferences sharedPref = myActivity.getSharedPreferences("preferences",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean("isLocUpdate", isLocUpdateService);
                editor.putBoolean("isAllNews", isAllNews);
                editor.putBoolean("isNewsUpdate",isNewsUpdateService);
                if(isLocUpdateService){
                    editor.putInt("LocInterval",locUpdateInterval);
                }
                if (isNewsUpdateService){
                    editor.putInt("NewsInterval",newsUpdateInterval);
                }
                if(!isAllNews) {
                    editor.putString("tripname", SpinnerValue);
                    editor.putInt("spinPos",SpinnerPosition);
                    try {
                        ArrayList<LatLng> listLatLng = myDbHelper.getLatLng(SpinnerValue);

                        ReportNewsFragment reportFragment = (ReportNewsFragment)myActivity.reportFrag;
                        if(reportFragment != null){
                            reportFragment.setRecordLatLngs(listLatLng);
                            reportFragment.mode = ReportNewsFragment.SHOW_DEPEND_RECORD;
                            reportFragment.showNewsWithOption(ReportNewsFragment.SHOW_DEPEND_RECORD);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(isAllNews){
                    ReportNewsFragment reportFragment = (ReportNewsFragment)myActivity.reportFrag;
                    if(reportFragment != null){
                        reportFragment.mode = ReportNewsFragment.SHOW_ALL_NEWS;
                        reportFragment.showNewsWithOption(ReportNewsFragment.SHOW_ALL_NEWS);
                    }
                }

                editor.commit();
                if(isLocUpdateService){
                    getActivity().startService(new Intent(getActivity(), UpdateLocationService.class));
                }else{
                    getActivity().stopService(new Intent(getActivity(), UpdateLocationService.class));
                }

                if(isNewsUpdateService){
                    myActivity.setIntervalMillis(newsUpdateInterval);
                    myActivity.scheduleAlarm();
                }else{
                    myActivity.cancelAlarm();
                }
                Toast.makeText(myActivity,"Preferences has been saved successfully.",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
